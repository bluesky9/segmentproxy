export interface Organization {
  name: string;
  plan: string;
  helpUrl: string;
  communityUrl: string;
  signoutUrl: string;
  id: number;
}

export interface OrganizationState {
  organization: Organization;
}
