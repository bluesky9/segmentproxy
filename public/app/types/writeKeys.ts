﻿import { OrgRole } from './acl';

export interface WriteKey {
  id: number;
  name: string;
  key: string;
}

export interface NewWriteKey {
  name: string;
  role: OrgRole;
}

export interface WriteKeysState {
  keys: WriteKey[];
  searchQuery: string;
  hasFetched: boolean;
}
