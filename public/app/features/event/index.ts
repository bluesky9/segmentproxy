import EventTypesCtrl from './EventTypesCtrl';
//import AdminEditUserCtrl from './AdminEditUserCtrl';
//import AdminListOrgsCtrl from './AdminListOrgsCtrl';
//import AdminEditOrgCtrl from './AdminEditOrgCtrl';
//import StyleGuideCtrl from './StyleGuideCtrl';

import coreModule from 'app/core/core_module';

/*
class EventSettingsCtrl {
  navModel: any;

  constructor($scope, backendSrv, navModelSrv) {
    this.navModel = navModelSrv.getNav('cfg', 'event', 'settings', 1);

    backendSrv.get('/api/admin/settings').then(settings => {
      $scope.settings = settings;
    });
  }
}
*/

class EventHomeCtrl {
  navModel: any;

  constructor(navModelSrv) {
    this.navModel = navModelSrv.getNav('cfg', 'event', 1);
  }
}

coreModule.controller('EventTypesCtrl', EventTypesCtrl);
//coreModule.controller('AdminEditUserCtrl', AdminEditUserCtrl);
//coreModule.controller('AdminListOrgsCtrl', AdminListOrgsCtrl);
//coreModule.controller('AdminEditOrgCtrl', AdminEditOrgCtrl);
//coreModule.controller('AdminSettingsCtrl', AdminSettingsCtrl);
coreModule.controller('EventHomeCtrl', EventHomeCtrl);
//coreModule.controller('StyleGuideCtrl', StyleGuideCtrl);
