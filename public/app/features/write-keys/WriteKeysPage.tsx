import React, { PureComponent } from 'react';
import ReactDOMServer from 'react-dom/server';
import { connect } from 'react-redux';
import { hot } from 'react-hot-loader';
import { NavModel, WriteKey, NewWriteKey } from 'app/types'; //, OrgRole
import { getNavModel } from 'app/core/selectors/navModel';
import { getWriteKeys, getWriteKeysCount } from './state/selectors';
import { loadWriteKeys, deleteWriteKey, setSearchQuery, addWriteKey } from './state/actions';
import PageHeader from 'app/core/components/PageHeader/PageHeader';
import PageLoader from 'app/core/components/PageLoader/PageLoader';
import SlideDown from 'app/core/components/Animations/SlideDown';
import WriteKeysAddedModal from './WriteKeysAddedModal';
import config from 'app/core/config';
import appEvents from 'app/core/app_events';
import EmptyListCTA from 'app/core/components/EmptyListCTA/EmptyListCTA';
import { DeleteButton } from '@grafana/ui';

export interface Props {
  navModel: NavModel;
  writeKeys: WriteKey[];
  searchQuery: string;
  hasFetched: boolean;
  loadWriteKeys: typeof loadWriteKeys;
  deleteWriteKey: typeof deleteWriteKey;
  setSearchQuery: typeof setSearchQuery;
  addWriteKey: typeof addWriteKey;
  writeKeysCount: number;
}

export interface State {
  isAdding: boolean;
  newWriteKey: NewWriteKey;
}

enum WriteKeyStateProps {
  Name = 'name',
}

const initialWriteKeyState = {
  name: '',
};

export class WriteKeysPage extends PureComponent<Props, any> {
  constructor(props) {
    super(props);
    this.state = { isAdding: false, newWriteKey: initialWriteKeyState };
  }

  componentDidMount() {
    this.fetchWriteKeys();
  }

  async fetchWriteKeys() {
    await this.props.loadWriteKeys();
  }

  onDeleteWriteKey(key: WriteKey) {
    this.props.deleteWriteKey(key.id);
  }

  onSearchQueryChange = evt => {
    this.props.setSearchQuery(evt.target.value);
  };

  onToggleAdding = () => {
    this.setState({ isAdding: !this.state.isAdding });
  };

  onAddWriteKey = async evt => {
    evt.preventDefault();

    const openModal = (writeKey: string) => {
      const rootPath = window.location.origin + config.appSubUrl;
      const modalTemplate = ReactDOMServer.renderToString(
        <WriteKeysAddedModal writeKey={writeKey} rootPath={rootPath} />
      );

      appEvents.emit('show-modal', {
        templateHtml: modalTemplate,
      });
    };

    this.props.addWriteKey(this.state.newWriteKey, openModal);
    this.setState((prevState: State) => {
      return {
        ...prevState,
        newWriteKey: initialWriteKeyState,
        isAdding: false,
      };
    });
  };

  onWriteKeyStateUpdate = (evt, prop: string) => {
    const value = evt.currentTarget.value;
    this.setState((prevState: State) => {
      const newWriteKey = {
        ...prevState.newWriteKey,
      };
      newWriteKey[prop] = value;

      return {
        ...prevState,
        newWriteKey: newWriteKey,
      };
    });
  };

  renderEmptyList() {
    const { isAdding } = this.state;
    return (
      <div className="page-container page-body">
        {!isAdding && (
          <EmptyListCTA
            model={{
              title: "You haven't added any Write Keys yet.",
              buttonIcon: 'gicon gicon-writekeys',
              buttonLink: '#',
              onClick: this.onToggleAdding,
              buttonTitle: ' New Write Key',
              proTip:
                'These write keys are used wheen seding events to segmentproxy.com. Look at Routing rules to see Segment.com write keys.',
              proTipLink: '',
              proTipLinkTitle: '',
              proTipTarget: '_blank',
            }}
          />
        )}
        {this.renderAddWriteKeyForm()}
      </div>
    );
  }

  renderAddWriteKeyForm() {
    const { newWriteKey, isAdding } = this.state;

    return (
      <SlideDown in={isAdding}>
        <div className="cta-form">
          <button className="cta-form__close btn btn-transparent" onClick={this.onToggleAdding}>
            <i className="fa fa-close" />
          </button>
          <h5>Add Write Key</h5>
          <form className="gf-form-group" onSubmit={this.onAddWriteKey}>
            <div className="gf-form-inline">
              <div className="gf-form max-width-21">
                <span className="gf-form-label">Key name</span>
                <input
                  type="text"
                  className="gf-form-input"
                  value={newWriteKey.name}
                  placeholder="Name"
                  onChange={evt => this.onWriteKeyStateUpdate(evt, WriteKeyStateProps.Name)}
                />
              </div>
              <div className="gf-form">
                <button className="btn gf-form-btn btn-success">Add</button>
              </div>
            </div>
          </form>
        </div>
      </SlideDown>
    );
  }

  renderWriteKeyList() {
    const { isAdding } = this.state;
    const { writeKeys, searchQuery } = this.props;

    return (
      <div className="page-container page-body">
        <div className="page-action-bar">
          <div className="gf-form gf-form--grow">
            <label className="gf-form--has-input-icon gf-form--grow">
              <input
                type="text"
                className="gf-form-input"
                placeholder="Search write keys"
                value={searchQuery}
                onChange={this.onSearchQueryChange}
              />
              <i className="gf-form-input-icon fa fa-search" />
            </label>
          </div>

          <div className="page-action-bar__spacer" />
          <button className="btn btn-success pull-right" onClick={this.onToggleAdding} disabled={isAdding}>
            <i className="fa fa-plus" /> Add Write Key
          </button>
        </div>

        {this.renderAddWriteKeyForm()}

        <h3 className="page-heading">Existing Keys</h3>
        <table className="filter-table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Writekey</th>
              <th style={{ width: '34px' }} />
            </tr>
          </thead>
          {writeKeys.length > 0 ? (
            <tbody>
              {writeKeys.map(key => {
                return (
                  <tr key={key.id}>
                    <td>{key.name}</td>
                    <td>{key.key}</td>
                    <td>
                      <DeleteButton onConfirm={() => this.onDeleteWriteKey(key)} />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          ) : null}
        </table>
      </div>
    );
  }

  render() {
    const { hasFetched, navModel, writeKeysCount } = this.props;
    return (
      <div>
        <PageHeader model={navModel} />
        {hasFetched ? (
          writeKeysCount > 0 ? (
            this.renderWriteKeyList()
          ) : (
            this.renderEmptyList()
          )
        ) : (
          <PageLoader pageName="Write keys" />
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    navModel: getNavModel(state.navIndex, 'writekeys'),
    writeKeys: getWriteKeys(state.writeKeys),
    searchQuery: state.writeKeys.searchQuery,
    writeKeysCount: getWriteKeysCount(state.writeKeys),
    hasFetched: state.writeKeys.hasFetched,
  };
}

const mapDispatchToProps = {
  loadWriteKeys,
  deleteWriteKey,
  setSearchQuery,
  addWriteKey,
};

export default hot(module)(connect(mapStateToProps, mapDispatchToProps)(WriteKeysPage));
