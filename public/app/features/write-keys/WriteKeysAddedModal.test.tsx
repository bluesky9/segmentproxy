﻿import React from 'react';
import { shallow } from 'enzyme';
import { WriteKeysAddedModal, Props } from './WriteKeysAddedModal';

const setup = (propOverrides?: object) => {
  const props: Props = {
    writeKey: 'write key test',
    rootPath: 'test/path',
  };

  Object.assign(props, propOverrides);

  const wrapper = shallow(<WriteKeysAddedModal {...props} />);

  return {
    wrapper,
  };
};

describe('Render', () => {
  it('should render component', () => {
    const { wrapper } = setup();
    expect(wrapper).toMatchSnapshot();
  });
});
