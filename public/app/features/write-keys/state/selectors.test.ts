﻿import { getWriteKeys } from './selectors';
import { getMultipleMockKeys } from '../__mocks__/writeKeysMock';
import { WriteKeysState } from 'app/types';

describe('Write Keys selectors', () => {
  describe('Get Write Keys', () => {
    const mockKeys = getMultipleMockKeys(5);

    it('should return all keys if no search query', () => {
      const mockState: WriteKeysState = { keys: mockKeys, searchQuery: '', hasFetched: false };

      const keys = getWriteKeys(mockState);

      expect(keys).toEqual(mockKeys);
    });

    it('should filter keys if search query exists', () => {
      const mockState: WriteKeysState = { keys: mockKeys, searchQuery: '5', hasFetched: false };

      const keys = getWriteKeys(mockState);

      expect(keys.length).toEqual(1);
    });
  });
});
