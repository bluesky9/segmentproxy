﻿import { Action, ActionTypes } from './actions';
import { initialWriteKeysState, writeKeysReducer } from './reducers';
import { getMultipleMockKeys } from '../__mocks__/writeKeysMock';

describe('Write Keys reducer', () => {
  it('should set keys', () => {
    const payload = getMultipleMockKeys(4);

    const action: Action = {
      type: ActionTypes.LoadWriteKeys,
      payload,
    };

    const result = writeKeysReducer(initialWriteKeysState, action);

    expect(result.keys).toEqual(payload);
  });

  it('should set search query', () => {
    const payload = 'test query';

    const action: Action = {
      type: ActionTypes.SetWriteKeysSearchQuery,
      payload,
    };

    const result = writeKeysReducer(initialWriteKeysState, action);

    expect(result.searchQuery).toEqual('test query');
  });
});
