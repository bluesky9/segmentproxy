﻿import { ThunkAction } from 'redux-thunk';
import { getBackendSrv } from 'app/core/services/backend_srv';
import { StoreState, WriteKey } from 'app/types';

export enum ActionTypes {
  LoadWriteKeys = 'LOAD_WRITE_KEYS',
  SetWriteKeysSearchQuery = 'SET_WRITE_KEYS_SEARCH_QUERY',
}

export interface LoadWriteKeysAction {
  type: ActionTypes.LoadWriteKeys;
  payload: WriteKey[];
}

export interface SetSearchQueryAction {
  type: ActionTypes.SetWriteKeysSearchQuery;
  payload: string;
}

export type Action = LoadWriteKeysAction | SetSearchQueryAction;

type ThunkResult<R> = ThunkAction<R, StoreState, undefined, Action>;

const writeKeysLoaded = (writeKeys: WriteKey[]): LoadWriteKeysAction => ({
  type: ActionTypes.LoadWriteKeys,
  payload: writeKeys,
});

export function addWriteKey(writeKey: WriteKey, openModal: (key: string) => void): ThunkResult<void> {
  return async dispatch => {
    const result = await getBackendSrv().post('/api/auth/writekeys', writeKey);
    dispatch(setSearchQuery(''));
    dispatch(loadWriteKeys());
    openModal(result.key);
  };
}

export function loadWriteKeys(): ThunkResult<void> {
  return async dispatch => {
    const response = await getBackendSrv().get('/api/auth/writekeys');
    dispatch(writeKeysLoaded(response));
  };
}

export function deleteWriteKey(id: number): ThunkResult<void> {
  return async dispatch => {
    getBackendSrv()
      .delete('/api/auth/writekeys/' + id)
      .then(dispatch(loadWriteKeys()));
  };
}

export const setSearchQuery = (searchQuery: string): SetSearchQueryAction => ({
  type: ActionTypes.SetWriteKeysSearchQuery,
  payload: searchQuery,
});
