﻿import { WriteKeysState } from 'app/types';

export const getWriteKeysCount = (state: WriteKeysState) => state.keys.length;

export const getWriteKeys = (state: WriteKeysState) => {
  const regex = RegExp(state.searchQuery, 'i');

  return state.keys.filter(writeKey => {
    return regex.test(writeKey.name); // || regex.test(writeKey.id);
  });
};
