﻿import { WriteKeysState } from 'app/types';
import { Action, ActionTypes } from './actions';

export const initialWriteKeysState: WriteKeysState = {
  keys: [],
  searchQuery: '',
  hasFetched: false,
};

export const writeKeysReducer = (state = initialWriteKeysState, action: Action): WriteKeysState => {
  switch (action.type) {
    case ActionTypes.LoadWriteKeys:
      return { ...state, hasFetched: true, keys: action.payload };
    case ActionTypes.SetWriteKeysSearchQuery:
      return { ...state, searchQuery: action.payload };
  }
  return state;
};

export default {
  writeKeys: writeKeysReducer,
};
