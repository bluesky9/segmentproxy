﻿import { WriteKey } from 'app/types';

export const getMultipleMockKeys = (numberOfKeys: number): WriteKey[] => {
  const keys: WriteKey[] = [];
  for (let i = 1; i <= numberOfKeys; i++) {
    keys.push({
      id: i,
      name: `test-${i}`,
      key: 'some-${i}',
    });
  }

  return keys;
};

export const getMockKey = (): WriteKey => {
  return {
    id: 1,
    name: 'test',
    key: 'some',
  };
};
