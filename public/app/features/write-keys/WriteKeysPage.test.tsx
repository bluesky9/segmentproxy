import React from 'react';
import { shallow } from 'enzyme';
import { Props, WriteKeysPage } from './WriteKeysPage';
import { NavModel, WriteKey } from 'app/types';
import { getMultipleMockKeys, getMockKey } from './__mocks__/writeKeysMock';

const setup = (propOverrides?: object) => {
  const props: Props = {
    navModel: {} as NavModel,
    writeKeys: [] as WriteKey[],
    searchQuery: '',
    hasFetched: false,
    loadWriteKeys: jest.fn(),
    deleteWriteKey: jest.fn(),
    setSearchQuery: jest.fn(),
    addWriteKey: jest.fn(),
    writeKeysCount: 0,
  };

  Object.assign(props, propOverrides);

  const wrapper = shallow(<WriteKeysPage {...props} />);
  const instance = wrapper.instance() as WriteKeysPage;

  return {
    wrapper,
    instance,
  };
};

describe('Render', () => {
  it('should render Write keys table if there are any keys', () => {
    const { wrapper } = setup({
      writeKeys: getMultipleMockKeys(5),
      writeKeysCount: 5,
    });

    expect(wrapper).toMatchSnapshot();
  });

  it('should render CTA if there are no Write keys', () => {
    const { wrapper } = setup({
      writeKeys: getMultipleMockKeys(0),
      writeKeysCount: 0,
      hasFetched: true,
    });

    expect(wrapper).toMatchSnapshot();
  });
});

describe('Life cycle', () => {
  it('should call loadWriteKeys', () => {
    const { instance } = setup();

    instance.componentDidMount();

    expect(instance.props.loadWriteKeys).toHaveBeenCalled();
  });
});

describe('Functions', () => {
  describe('Delete team', () => {
    it('should call delete team', () => {
      const { instance } = setup();
      instance.onDeleteWriteKey(getMockKey());
      expect(instance.props.deleteWriteKey).toHaveBeenCalledWith(1);
    });
  });

  describe('on search query change', () => {
    it('should call setSearchQuery', () => {
      const { instance } = setup();
      const mockEvent = { target: { value: 'test' } };

      instance.onSearchQueryChange(mockEvent);

      expect(instance.props.setSearchQuery).toHaveBeenCalledWith('test');
    });
  });
});
