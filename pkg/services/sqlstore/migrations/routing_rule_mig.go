package migrations

import (
	. "github.com/grafana/grafana/pkg/services/sqlstore/migrator"
)

func addRoutingMigration(mg *Migrator) {

	var routingV1 = Table{
		Name: "routing_rule",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "org_id", Type: DB_BigInt, Nullable: true},

			{Name: "active", Type: DB_Bool, Nullable: false},
			{Name: "name", Type: DB_NVarchar, Length: 190, Nullable: true},
			{Name: "filter", Type: DB_Text, Nullable: true},
			{Name: "augmentation", Type: DB_Text, Nullable: true},

			{Name: "destination", Type: DB_NVarchar, Length: 20, Nullable: true},
			{Name: "settings", Type: DB_Text, Nullable: true},
			{Name: "write_key", Type: DB_NVarchar, Length: 50, Nullable: true},

			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id"}, Type: IndexType},
		},
	}
	mg.AddMigration("create routing_rule table v1", NewAddTableMigration(routingV1))

	//-------  indexes ------------------
	addTableIndicesMigrations(mg, "v1", routingV1)

}
