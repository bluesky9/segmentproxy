package migrations

import (
	. "github.com/grafana/grafana/pkg/services/sqlstore/migrator"
)

func addEventtypeMigration(mg *Migrator) {

	var eventtypeV1 = Table{
		Name: "event_type",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "org_id", Type: DB_BigInt, Nullable: true},

			{Name: "active", Type: DB_Bool, Nullable: false},
			{Name: "type", Type: DB_NVarchar, Length: 20, Nullable: false},
			{Name: "name", Type: DB_NVarchar, Length: 190, Nullable: true},
			{Name: "template", Type: DB_Text, Nullable: true},
			{Name: "retention", Type: DB_BigInt, Nullable: true},

			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id"}, Type: IndexType},
			{Cols: []string{"org_id", "type", "name"}, Type: UniqueIndex},
		},
	}
	mg.AddMigration("create event_type table v1", NewAddTableMigration(eventtypeV1))

	//-------  indexes ------------------
	addTableIndicesMigrations(mg, "v1", eventtypeV1)

}
