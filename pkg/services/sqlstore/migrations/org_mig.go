package migrations

import . "github.com/grafana/grafana/pkg/services/sqlstore/migrator"

func addOrgMigrations(mg *Migrator) {
	orgV1 := Table{
		Name: "org",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "version", Type: DB_Int, Nullable: false},
			{Name: "name", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "plan", Type: DB_NVarchar, Length: 20, Nullable: true},

			//Address
			{Name: "address1", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "address2", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "city", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "state", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "zip_code", Type: DB_NVarchar, Length: 50, Nullable: true},
			{Name: "country", Type: DB_NVarchar, Length: 255, Nullable: true},

			//Billing
			{Name: "billing_contact", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "billing_phone", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "billing_email", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "billing_id", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "monthly_price", Type: DB_Double, Nullable: true},

			//Solution & Appearance
			{Name: "solution_signature", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "domain_mapping", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "partition", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "sql_predicate", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "asset_prefix", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "logo", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "logo_full", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "accent_color_light", Type: DB_NVarchar, Length: 50, Nullable: true},
			{Name: "accent_color_dark", Type: DB_NVarchar, Length: 50, Nullable: true},
			{Name: "themefile_light", Type: DB_NVarchar, Length: 50, Nullable: true},
			{Name: "themefile_dark", Type: DB_NVarchar, Length: 50, Nullable: true},
			{Name: "help_url", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "community_url", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "signout_redirect_url", Type: DB_NVarchar, Length: 255, Nullable: true},

			//Settings
			{Name: "users_limit", Type: DB_Int, Nullable: false, Default: "10"},
			{Name: "dashboards_limit", Type: DB_Int, Nullable: false, Default: "20"},
			{Name: "data_sources_limit", Type: DB_Int, Nullable: false, Default: "1"},
			{Name: "api_keys_limit", Type: DB_Int, Nullable: false, Default: "5"},
			{Name: "write_keys_limit", Type: DB_Int, Nullable: false, Default: "5"},
			{Name: "alerts_limit", Type: DB_Int, Nullable: false, Default: "0"},
			{Name: "route_limit", Type: DB_Int, Nullable: false, Default: "5"},
			{Name: "statement_limit", Type: DB_Int, Nullable: false, Default: "0"},
			{Name: "daily_rate_limit", Type: DB_BigInt, Nullable: false, Default: "-1"},

			//Features
			{Name: "advanced_routing", Type: DB_Bool, Nullable: false, Default: "0"},
			{Name: "exploring", Type: DB_Bool, Nullable: false, Default: "0"},
			{Name: "entity_graph", Type: DB_Bool, Nullable: false, Default: "0"},
			{Name: "streaming_dashboards", Type: DB_Bool, Nullable: false, Default: "0"},
			{Name: "enterprise_search", Type: DB_Bool, Nullable: false, Default: "0"},
			{Name: "sign_up", Type: DB_Bool, Nullable: false, Default: "0"},
			{Name: "basic_auth", Type: DB_Bool, Nullable: true, Default: "0"},
			{Name: "login_form", Type: DB_Bool, Nullable: true, Default: "0"},

			//Authentication
			{Name: "oauth_auto_login", Type: DB_Bool, Nullable: false, Default: "0"},
			{Name: "github_client_id", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "github_client_secret", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "gitlab_client_id", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "gitlab_client_secret", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "google_client_id", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "google_client_secret", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "oauth_client_id", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "oauth_client_secret", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "oauth_auth_url", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "oauth_token_url", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "oauth_api_url", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "oauth_tls_client_cert", Type: DB_Blob, Nullable: true},
			{Name: "oauth_tls_client_key", Type: DB_Blob, Nullable: true},
			{Name: "oauth_send_via_post", Type: DB_Bool, Nullable: true},
			{Name: "oauth_team_ids", Type: DB_Text, Nullable: true},

			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"name"}, Type: UniqueIndex},
			{Cols: []string{"domain_mapping"}, Type: IndexType},
			{Cols: []string{"billing_id"}, Type: IndexType},
		},
	}

	// add org v1
	mg.AddMigration("create org table v1", NewAddTableMigration(orgV1))
	addTableIndicesMigrations(mg, "v1", orgV1)

	orgUserV1 := Table{
		Name: "org_user",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "org_id", Type: DB_BigInt},
			{Name: "user_id", Type: DB_BigInt},
			{Name: "role", Type: DB_NVarchar, Length: 20},
			{Name: "created", Type: DB_DateTime},
			{Name: "updated", Type: DB_DateTime},
			{Name: "partition", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "sql_predicate", Type: DB_NVarchar, Length: 255, Nullable: true},
		},
		Indices: []*Index{
			{Cols: []string{"org_id"}},
			{Cols: []string{"org_id", "user_id"}, Type: UniqueIndex},
		},
	}

	//-------  org_user table -------------------
	mg.AddMigration("create org_user table v1", NewAddTableMigration(orgUserV1))
	addTableIndicesMigrations(mg, "v1", orgUserV1)

	mg.AddMigration("Update org table charset", NewTableCharsetMigration("org", []*Column{
		{Name: "name", Type: DB_NVarchar, Length: 190, Nullable: false},
		{Name: "address1", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "address2", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "city", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "state", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "zip_code", Type: DB_NVarchar, Length: 50, Nullable: true},
		{Name: "country", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "billing_email", Type: DB_NVarchar, Length: 255, Nullable: true},
	}))

	mg.AddMigration("Update org_user table charset", NewTableCharsetMigration("org_user", []*Column{
		{Name: "role", Type: DB_NVarchar, Length: 20},
	}))

	const migrateReadOnlyViewersToViewers = `UPDATE org_user SET role = 'Viewer' WHERE role = 'Read Only Editor'`
	mg.AddMigration("Migrate all Read Only Viewers to Viewers", NewRawSqlMigration(migrateReadOnlyViewersToViewers))
}
