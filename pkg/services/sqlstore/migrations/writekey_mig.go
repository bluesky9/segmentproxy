package migrations

import . "github.com/grafana/grafana/pkg/services/sqlstore/migrator"

func addWriteKeyMigrations(mg *Migrator) {
	writeKeyV1 := Table{
		Name: "write_key",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "account_id", Type: DB_BigInt, Nullable: false},
			{Name: "name", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "key", Type: DB_Varchar, Length: 64, Nullable: false},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"account_id"}},
			{Cols: []string{"key"}, Type: UniqueIndex},
			{Cols: []string{"account_id", "name"}, Type: UniqueIndex},
		},
	}

	// create table
	mg.AddMigration("create write_key table", NewAddTableMigration(writeKeyV1))
	// create indices
	mg.AddMigration("add index write_key.account_id", NewAddIndexMigration(writeKeyV1, writeKeyV1.Indices[0]))
	mg.AddMigration("add index write_key.key", NewAddIndexMigration(writeKeyV1, writeKeyV1.Indices[1]))
	mg.AddMigration("add index write_key.account_id_name", NewAddIndexMigration(writeKeyV1, writeKeyV1.Indices[2]))

	// ---------------------
	// account -> org changes

	// drop indexes
	addDropAllIndicesMigrations(mg, "v1", writeKeyV1)
	// rename table
	addTableRenameMigration(mg, "write_key", "write_key_v1", "v1")

	writeKeyV2 := Table{
		Name: "write_key",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "org_id", Type: DB_BigInt, Nullable: false},
			{Name: "name", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "key", Type: DB_Varchar, Length: 190, Nullable: false},
			{Name: "created", Type: DB_DateTime, Nullable: false},
			{Name: "updated", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id"}},
			{Cols: []string{"key"}, Type: UniqueIndex},
			{Cols: []string{"org_id", "name"}, Type: UniqueIndex},
		},
	}

	// create v2 table
	mg.AddMigration("create write_key table v2", NewAddTableMigration(writeKeyV2))

	// add v2 indíces
	addTableIndicesMigrations(mg, "v2", writeKeyV2)

	//------- copy data from v1 to v2 -------------------
	mg.AddMigration("copy write_key v1 to v2", NewCopyTableDataMigration("write_key", "write_key_v1", map[string]string{
		"id":      "id",
		"org_id":  "account_id",
		"name":    "name",
		"key":     "key",
		"created": "created",
		"updated": "updated",
	}))

	mg.AddMigration("Drop old table write_key_v1", NewDropTableMigration("write_key_v1"))

	mg.AddMigration("Update write_key table charset", NewTableCharsetMigration("write_key", []*Column{
		{Name: "name", Type: DB_NVarchar, Length: 190, Nullable: false},
		{Name: "key", Type: DB_Varchar, Length: 190, Nullable: false},
	}))
}
