package sqlstore

import (
	"github.com/go-xorm/xorm"
	"github.com/grafana/grafana/pkg/bus"
	m "github.com/grafana/grafana/pkg/models"
	"time"
)

func init() {
	bus.AddHandler("sql", GetStatements)
	bus.AddHandler("sql", GetAllStatements)
	bus.AddHandler("sql", AddStatement)
	bus.AddHandler("sql", DeleteStatementById)
	bus.AddHandler("sql", UpdateStatement)
	bus.AddHandler("sql", GetStatementById)
	bus.AddHandler("sql", GetStatementByName)
}

func GetStatementById(query *m.GetStatementByIdQuery) error {
	statement := m.Statement{OrgId: query.OrgId, Id: query.Id}
	has, err := x.Get(&statement)

	if err != nil {
		return err
	}

	if !has {
		return m.ErrStatementNotFound
	}

	query.Result = &statement
	return err
}

func GetStatementByName(query *m.GetStatementByNameQuery) error {
	statement := m.Statement{OrgId: query.OrgId, Name: query.Name}
	has, err := x.Get(&statement)

	if !has {
		return m.ErrStatementNotFound
	}

	query.Result = &statement
	return err
}

func GetStatements(query *m.GetStatementQuery) error {
	sess := x.Limit(5000, 0).Where("org_id=?", query.OrgId).Asc("name")

	query.Result = make([]*m.Statement, 0)
	return sess.Find(&query.Result)
}

func GetAllStatements(query *m.GetAllStatementsQuery) error {
	sess := x.Limit(5000, 0).Asc("name")

	query.Result = make([]*m.Statement, 0)
	return sess.Find(&query.Result)
}

func DeleteStatementById(cmd *m.DeleteStatementCommand) error {
	return inTransaction(func(sess *DBSession) error {
		var rawSql = "DELETE FROM data_source WHERE id=? and org_id=?"
		_, err := sess.Exec(rawSql, cmd.Id, cmd.OrgId)
		return err
	})
}

func AddStatement(cmd *m.AddStatementCommand) error {
	return inTransaction(func(sess *DBSession) error {
		existing := m.Statement{OrgId: cmd.OrgId, Name: cmd.Name}
		has, _ := sess.Get(&existing)

		if has {
			return m.ErrStatementNameExists
		}

		ds := &m.Statement{
			OrgId:     cmd.OrgId,
			Name:      cmd.Name,
			Type:      cmd.Type,
			Statement: cmd.Statement,
			Created:   time.Now(),
			Updated:   time.Now(),
		}

		if _, err := sess.Insert(ds); err != nil {
			return err
		}

		cmd.Result = ds
		return nil
	})
}

func UpdateStatement(cmd *m.UpdateStatementCommand) error {
	return inTransaction(func(sess *DBSession) error {
		ds := &m.Statement{
			Id:        cmd.Id,
			OrgId:     cmd.OrgId,
			Name:      cmd.Name,
			Type:      cmd.Type,
			Statement: cmd.Statement,
			Updated:   time.Now(),
		}
		var updateSession *xorm.Session
		_, err := updateSession.Update(ds)
		return err
	})
}
