package sqlstore

import (
	"context"
	"time"

	"github.com/grafana/grafana/pkg/bus"
	m "github.com/grafana/grafana/pkg/models"
)

func init() {
	bus.AddHandler("sql", GetWriteKeys)
	bus.AddHandler("sql", GetWriteKeyById)
	bus.AddHandler("sql", GetWriteKeyByName)
	bus.AddHandlerCtx("sql", DeleteWriteKeyCtx)
	bus.AddHandler("sql", AddWriteKey)
}

func GetWriteKeys(query *m.GetWriteKeysQuery) error {
	sess := x.Limit(100, 0).Where("org_id=?", query.OrgId).Asc("name")

	query.Result = make([]*m.WriteKey, 0)
	return sess.Find(&query.Result)
}

func DeleteWriteKeyCtx(ctx context.Context, cmd *m.DeleteWriteKeyCommand) error {
	return withDbSession(ctx, func(sess *DBSession) error {
		var rawSql = "DELETE FROM write_key WHERE id=? and org_id=?"
		_, err := sess.Exec(rawSql, cmd.Id, cmd.OrgId)
		return err
	})
}

func AddWriteKey(cmd *m.AddWriteKeyCommand) error {
	return inTransaction(func(sess *DBSession) error {
		t := m.WriteKey{
			OrgId:   cmd.OrgId,
			Name:    cmd.Name,
			Key:     cmd.Key,
			Created: time.Now(),
			Updated: time.Now(),
		}

		if _, err := sess.Insert(&t); err != nil {
			return err
		}
		cmd.Result = &t
		return nil
	})
}

func GetWriteKeyById(query *m.GetWriteKeyByIdQuery) error {
	var writekey m.WriteKey
	has, err := x.Id(query.WriteKeyId).Get(&writekey)

	if err != nil {
		return err
	} else if !has {
		return m.ErrInvalidWriteKey
	}

	query.Result = &writekey
	return nil
}

func GetWriteKeyByName(query *m.GetWriteKeyByNameQuery) error {
	var writekey m.WriteKey
	has, err := x.Where("org_id=? AND name=?", query.OrgId, query.KeyName).Get(&writekey)

	if err != nil {
		return err
	} else if !has {
		return m.ErrInvalidWriteKey
	}

	query.Result = &writekey
	return nil
}
