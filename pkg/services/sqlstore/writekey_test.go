package sqlstore

import (
	m "github.com/grafana/grafana/pkg/models"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestWriteKeyDataAccess(t *testing.T) {

	Convey("Testing Write Key data access", t, func() {
		InitTestDB(t)

		Convey("Given saved write key", func() {
			cmd := m.AddWriteKeyCommand{OrgId: 1, Name: "hello", Key: "asd"}
			err := AddWriteKey(&cmd)
			So(err, ShouldBeNil)

			Convey("Should be able to get key by name", func() {
				query := m.GetWriteKeyByNameQuery{KeyName: "hello", OrgId: 1}
				err = GetWriteKeyByName(&query)

				So(err, ShouldBeNil)
				So(query.Result, ShouldNotBeNil)
			})

		})
	})
}
