package sqlstore

import (
	"context"
	"strings"
	"time"

	"github.com/grafana/grafana/pkg/bus"
	m "github.com/grafana/grafana/pkg/models"
)

func init() {
	bus.AddHandler("sql", GetEventTypes)
	bus.AddHandler("sql", GetApiKeyById)
	bus.AddHandler("sql", GetApiKeyByName)
	bus.AddHandler("sql", SearchEventTypes)
	bus.AddHandlerCtx("sql", DeleteApiKeyCtx)
	bus.AddHandler("sql", AddApiKey)
}

func GetEventTypes(query *m.GetEventTypesQuery) error {
	sess := x.Limit(100, 0).Where("org_id=?", query.OrgId).Asc("name")

	query.Result = make([]*m.EventType, 0)
	return sess.Find(&query.Result)
}

func DeleteEventTypeCtx(ctx context.Context, cmd *m.DeleteEventTypeCommand) error {
	return withDbSession(ctx, func(sess *DBSession) error {
		var rawSql = "DELETE FROM event_type WHERE id=? and org_id=?"
		_, err := sess.Exec(rawSql, cmd.Id, cmd.OrgId)
		return err
	})
}

func AddEventType(cmd *m.AddEventTypeCommand) error {
	return inTransaction(func(sess *DBSession) error {
		t := m.EventType{
			OrgId:   cmd.OrgId,
			Name:    cmd.Name,
			Type:    cmd.Type,
			Created: time.Now(),
			Updated: time.Now(),
		}

		if _, err := sess.Insert(&t); err != nil {
			return err
		}
		cmd.Result = &t
		return nil
	})
}

func GetEventTypeById(query *m.GetEventTypeByIdQuery) error {
	var eventType m.EventType
	has, err := x.Id(query.EventTypeId).Get(&eventType)

	if err != nil {
		return err
	} else if !has {
		return m.ErrInvalidEventType
	}

	query.Result = &eventType
	return nil
}

func GetEventTypeByName(query *m.GetEventTypeByNameQuery) error {
	var eventType m.EventType
	has, err := x.Where("org_id=? AND name=?", query.OrgId, query.Name).Get(&eventType)

	if err != nil {
		return err
	} else if !has {
		return m.ErrInvalidApiKey
	}

	query.Result = &eventType
	return nil
}

func SearchEventTypes(query *m.SearchEventTypeQuery) error {
	query.Result = m.SearchEventTypeQueryResult{
		EventTypes: make([]*m.EventTypeSearchHitDTO, 0),
	}

	queryWithWildcards := "%" + query.Query + "%"

	whereConditions := make([]string, 0)
	whereParams := make([]interface{}, 0)
	sess := x.Table("event_type")

	if query.OrgId > 0 {
		whereConditions = append(whereConditions, "org_id = ?")
		whereParams = append(whereParams, query.OrgId)
	}

	if query.Query != "" {
		whereConditions = append(whereConditions, "(name "+dialect.LikeStr()+" ? OR type "+dialect.LikeStr()+" ?)")
		whereParams = append(whereParams, queryWithWildcards, queryWithWildcards)
	}

	if len(whereConditions) > 0 {
		sess.Where(strings.Join(whereConditions, " AND "), whereParams...)
	}

	offset := query.Limit * (query.Page - 1)
	sess.Limit(query.Limit, offset)
	sess.Cols("id", "type", "name", "active")
	if err := sess.Find(&query.Result.EventTypes); err != nil {
		return err
	}

	// get total
	user := m.User{}
	countSess := x.Table("event_type")

	if len(whereConditions) > 0 {
		countSess.Where(strings.Join(whereConditions, " AND "), whereParams...)
	}

	count, err := countSess.Count(&user)
	query.Result.TotalCount = count

	return err
}