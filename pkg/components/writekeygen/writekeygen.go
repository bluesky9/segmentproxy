package writekeygen

import (
	"encoding/base64"
	"encoding/json"
	"errors"

	"github.com/grafana/grafana/pkg/util"
)

var ErrInvalidWriteKey = errors.New("Invalid Write Key")

type KeyGenResult struct {
	HashedKey    string
	ClientSecret string
}

type WriteKeyJson struct {
	Key   string `json:"k"`
	Name  string `json:"n"`
	OrgId int64  `json:"id"`
}

func New(orgId int64, name string) KeyGenResult {
	jsonKey := WriteKeyJson{}

	jsonKey.OrgId = orgId
	jsonKey.Name = name
	jsonKey.Key = util.GetRandomString(32)

	result := KeyGenResult{}
	result.HashedKey = util.EncodePassword(jsonKey.Key, name)

	jsonString, _ := json.Marshal(jsonKey)

	result.ClientSecret = base64.StdEncoding.EncodeToString(jsonString)
	return result
}

func Decode(keyString string) (*WriteKeyJson, error) {
	jsonString, err := base64.StdEncoding.DecodeString(keyString)
	if err != nil {
		return nil, ErrInvalidWriteKey
	}

	var keyObj WriteKeyJson
	err = json.Unmarshal(jsonString, &keyObj)
	if err != nil {
		return nil, ErrInvalidWriteKey
	}

	return &keyObj, nil
}

func IsValid(key *WriteKeyJson, hashedKey string) bool {
	check := util.EncodePassword(key.Key, key.Name)
	return check == hashedKey
}
