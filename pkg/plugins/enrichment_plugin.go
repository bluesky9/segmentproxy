package plugins

import "encoding/json"

type EnrichmentPlugin struct {
	FrontendPluginBase
}

func (p *EnrichmentPlugin) Load(decoder *json.Decoder, pluginDir string) error {
	if err := decoder.Decode(&p); err != nil {
		return err
	}

	if err := p.registerPlugin(pluginDir); err != nil {
		return err
	}

	Enrichments[p.Id] = p
	return nil
}
