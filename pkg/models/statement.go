package models

import (
	"errors"
	"time"
)

var (
	ErrInvalidStatement            = errors.New("Invalid Statement")
	ErrStatementNotFound           = errors.New("Statement not found")
	ErrStatementNameExists         = errors.New("Statement with same name already exists")
	ErrStatementUpdatingOldVersion = errors.New("Trying to update old version of a statement")
	ErrStatementIsReadOnly         = errors.New("Statment is readonly. Can only be updated from configuration.")
	ErrStatementAccessDenied       = errors.New("Statment access denied")
)

type Statement struct {
	Id        int64
	OrgId     int64
	Active    bool
	Type      string
	Name      string
	Statement string
	Created   time.Time
	Updated   time.Time
}

// ---------------------
// COMMANDS
type AddStatementCommand struct {
	Name      string     `json:"name" binding:"Required"`
	Type      string     `json:"type" binding:"Required"`
	Statement string     `json:"statement"`
	OrgId     int64      `json:"-"`
	Result    *Statement `json:"-"`
}

type UpdateStatementCommand struct {
	Id        int64  `json:"id"`
	Type      string `json:"type"`
	Name      string `json:"name"`
	Active    bool   `json:"active"`
	OrgId     int64  `json:"-"`
	Statement string `json:"statement"`

	Result *Statement
}

type DeleteStatementCommand struct {
	Id    int64 `json:"id"`
	OrgId int64 `json:"-"`
}

// ----------------------
// QUERIES

type GetAllStatementsQuery struct {
	Result []*Statement
}

type GetStatementQuery struct {
	OrgId  int64
	Result []*Statement
}

type GetStatementByNameQuery struct {
	Name   string
	OrgId  int64
	Result *Statement
}

type GetStatementByIdQuery struct {
	Id     int64
	OrgId  int64
	Result *Statement
}

// ------------------------
// DTO & Projections

type StatementDTO struct {
	Id        int64  `json:"id"`
	Name      string `json:"name"`
	Type      string `json:"type"`
	Active    bool   `json:"active"`
	Statement string `json:"statement"`
}
