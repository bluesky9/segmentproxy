package models

import (
	"errors"
	"time"
)

var ErrInvalidWriteKey = errors.New("Invalid Write Key")

type WriteKey struct {
	Id      int64
	OrgId   int64
	Name    string
	Key     string
	Created time.Time
	Updated time.Time
}

// ---------------------
// COMMANDS
type AddWriteKeyCommand struct {
	Name  string `json:"name" binding:"Required"`
	OrgId int64  `json:"-"`
	Key   string `json:"-"`

	Result *WriteKey `json:"-"`
}

type UpdateWriteKeyCommand struct {
	Id    int64  `json:"id"`
	Name  string `json:"name"`
	OrgId int64  `json:"-"`
}

type DeleteWriteKeyCommand struct {
	Id    int64 `json:"id"`
	OrgId int64 `json:"-"`
}

// ----------------------
// QUERIES

type GetWriteKeysQuery struct {
	OrgId  int64
	Result []*WriteKey
}

type GetWriteKeyByNameQuery struct {
	KeyName string
	OrgId   int64
	Result  *WriteKey
}

type GetWriteKeyByIdQuery struct {
	WriteKeyId int64
	Result     *WriteKey
}

// ------------------------
// DTO & Projections

type WriteKeyDTO struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
	Key  string `json:"key"`
}
