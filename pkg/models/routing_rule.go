package models

import (
	"errors"
	"time"
)

var ErrInvalidRoutingRule = errors.New("Invalid Routing Rule")

type RoutingRule struct {
	Id        int64
	OrgId     int64
	Active    bool
	Type      string
	Name      string
	Template  string
	Retention int64
	Created   time.Time
	Updated   time.Time
}

// ---------------------
// COMMANDS
type AddRoutingRuleCommand struct {
	Name   string       `json:"name" binding:"Required"`
	Type   string       `json:"type" binding:"Required"`
	OrgId  int64        `json:"-"`
	Result *RoutingRule `json:"-"`
}

type UpdateRoutingRuleCommand struct {
	Id        int64  `json:"id"`
	Type      string `json:"type"`
	Name      string `json:"name"`
	Template  string `json:"template"`
	Retention int64  `json:"retention"`
	Active    bool   `json:"active"`
	OrgId     int64  `json:"-"`
}

type DeleteRoutingRuleCommand struct {
	Id    int64 `json:"id"`
	OrgId int64 `json:"-"`
}

// ----------------------
// QUERIES

type GetRoutingRulesQuery struct {
	OrgId  int64
	Result []*RoutingRule
}

type GetRoutingRuleByNameQuery struct {
	Name   string
	OrgId  int64
	Result *RoutingRule
}

type GetRoutingRuleByIdQuery struct {
	RoutingRuleId int64
	Result        *RoutingRule
}

// ------------------------
// DTO & Projections

type RoutingRuleDTO struct {
	Id        int64  `json:"id"`
	Name      string `json:"name"`
	Type      string `json:"type"`
	Active    bool   `json:"active"`
	Template  string `json:"template"`
	Retention int64  `json:"retention"`
}
