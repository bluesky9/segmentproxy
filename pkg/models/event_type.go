package models

import (
	"errors"
	"time"
)

var ErrInvalidEventType = errors.New("Invalid Event Type")

type EventType struct {
	Id        int64
	OrgId     int64
	Active    bool
	Type      string
	Name      string
	Template  string
	Retention int64
	Created   time.Time
	Updated   time.Time
}

// ---------------------
// COMMANDS
type AddEventTypeCommand struct {
	Name   string     `json:"name" binding:"Required"`
	Type   string     `json:"type" binding:"Required"`
	OrgId  int64      `json:"-"`
	Result *EventType `json:"-"`
}

type UpdateEventTypeCommand struct {
	Id        int64  `json:"id"`
	Type      string `json:"type"`
	Name      string `json:"name"`
	Template  string `json:"template"`
	Retention int64  `json:"retention"`
	Active    bool   `json:"active"`
	OrgId     int64  `json:"-"`
}

type DeleteEventTypeCommand struct {
	Id    int64 `json:"id"`
	OrgId int64 `json:"-"`
}

// ----------------------
// QUERIES

type SearchEventTypeQuery struct {
	OrgId int64
	Query string
	Page  int
	Limit int

	Result SearchEventTypeQueryResult
}

type GetEventTypesQuery struct {
	OrgId  int64
	Result []*EventType
}

type GetEventTypeByNameQuery struct {
	Name   string
	OrgId  int64
	Result *EventType
}

type GetEventTypeByIdQuery struct {
	EventTypeId int64
	Result      *EventType
}

// ------------------------
// DTO & Projections

type EventTypeDTO struct {
	Id        int64  `json:"id"`
	Name      string `json:"name"`
	Type      string `json:"type"`
	Active    bool   `json:"active"`
	Template  string `json:"template"`
	Retention int64  `json:"retention"`
}

type SearchEventTypeQueryResult struct {
	TotalCount int64                    `json:"totalCount"`
	EventTypes []*EventTypeSearchHitDTO `json:"eventTypes"`
	Page       int                      `json:"page"`
	PerPage    int                      `json:"perPage"`
}

type EventTypeSearchHitDTO struct {
	Id     int64  `json:"id"`
	Name   string `json:"name"`
	Type   string `json:"type"`
	Active bool   `json:"active"`
}
