package models

import (
	"errors"
	"time"
)

// Typed errors
var (
	ErrOrgNotFound  = errors.New("Organization not found")
	ErrOrgNameTaken = errors.New("Organization name is taken")
)

type Org struct {
	Id      int64
	Version int
	Name    string

	Address1 string
	Address2 string
	City     string
	ZipCode  string
	State    string
	Country  string

	Created time.Time
	Updated time.Time

	Partition         string
	SqlPredicate      string
	AssetPrefix       string
	SolutionSignature string
	DomainMapping     string
}

// ---------------------
// COMMANDS

type CreateOrgCommand struct {
	Name              string `json:"name" binding:"Required"`
	Partition         string `json:"partition" binding:"Required"`
	SolutionSignature string `json:"solution_signature"`
	SqlPredicate      string `json:"sql_predicate"`
	DomainMapping     string `json:"domain_mapping"`
	AssetPrefix       string `json:"asset_prefix"`
	Plan              string `json:"plan"`
	// initial admin user for account
	UserId int64 `json:"-"`
	Result Org   `json:"-"`
}

type DeleteOrgCommand struct {
	Id int64
}

type UpdateOrgCommand struct {
	Name               string
	Partition          string
	SolutionSignature  string
	DomainMapping      string
	AssetPrefix        string
	Plan               string
	SqlPredicate       string
	OrgId              int64
	AccentColorLight   string
	AccentColorDark    string
	HelpUrl            string
	CommunityUrl       string
	SignoutUrl         string
	OauthAutoLogin     string
	GithubClientId     string
	GithubClientSecret string
	GitlabClientId     string
	GitlabClientSecret string
	GoogleClientId     string
	GoogleClientSecret string
	OauthClientId      string
	OauthClientSecret  string
	OauthAuthUrl       string
	OauthTokenUrl      string
	OauthApiUrl        string
	OauthTlsClientCert string
	OauthTlsClientKey  string
	OauthSendViaPost   string
}

type UpdateOrgAddressCommand struct {
	OrgId int64
	Address
}

type GetOrgByIdQuery struct {
	Id     int64
	Result *Org
}

type GetOrgByDomainQuery struct {
	Domain string
	Result *Org
}

type GetOrgByNameQuery struct {
	Name   string
	Result *Org
}

type SearchOrgsQuery struct {
	Query string
	Name  string
	Limit int
	Page  int

	Result []*OrgDTO
}

type OrgDTO struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}

type OrgDetailsDTO struct {
	Id                int64   `json:"id"`
	Name              string  `json:"name"`
	SolutionSignature string  `json:"solution_signature"`
	DomainMapping     string  `json:"domain_mapping"`
	AssetPrefix       string  `json:"asset_prefix"`
	Partition         string  `json:"partition"`
	SqlPredicate      string  `json:"sql_predicate"`
	Plan              string  `json:"plan"`
	AccentColorLight  string  `json:"accent_color_light"`
	AccentColorDark   string  `json:"accent_color_dark"`
	HelpUrl           string  `json:"help_url"`
	CommunityUrl      string  `json:"community_url"`
	SignoutUrl        string  `json:"signout_url"`
	Address           Address `json:"address"`
}

type UserOrgDTO struct {
	OrgId        int64    `json:"orgId"`
	Name         string   `json:"name"`
	Role         RoleType `json:"role"`
	Partition    string   `json:"partition"`
	SqlPredicate string   `json:"sql_predicate"`
	DomainMapping string  `json:"domain_mapping"`
}
