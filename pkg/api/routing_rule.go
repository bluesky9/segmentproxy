package api

import (
	"github.com/grafana/grafana/pkg/api/dtos"
	"github.com/grafana/grafana/pkg/bus"
	m "github.com/grafana/grafana/pkg/models"
)

func GetRoutingRules(c *m.ReqContext) Response {
	query := m.GetRoutingRulesQuery{OrgId: c.OrgId}

	if err := bus.Dispatch(&query); err != nil {
		return Error(500, "Failed to list Routing Rules", err)
	}

	result := make([]*m.RoutingRuleDTO, len(query.Result))
	for i, t := range query.Result {
		result[i] = &m.RoutingRuleDTO{
			Id:        t.Id,
			Name:      t.Name,
			Type:      t.Type,
			Active:    t.Active,
			Template:  t.Template,
			Retention: t.Retention,
		}
	}

	return JSON(200, result)
}

func DeleteRoutingRule(c *m.ReqContext) Response {
	id := c.ParamsInt64(":id")

	cmd := &m.DeleteRoutingRuleCommand{Id: id, OrgId: c.OrgId}

	err := bus.Dispatch(cmd)
	if err != nil {
		return Error(500, "Failed to delete Routing Rule", err)
	}

	return Success("Routing Rule key deleted")
}

func AddRoutingRule(c *m.ReqContext, cmd m.AddRoutingRuleCommand) Response {
	cmd.OrgId = c.OrgId

	if err := bus.Dispatch(&cmd); err != nil {
		return Error(500, "Failed to add Routing Rule", err)
	}

	result := &dtos.NewRoutingRuleResult{
		Name: cmd.Result.Name,
	}

	return JSON(200, result)
}
