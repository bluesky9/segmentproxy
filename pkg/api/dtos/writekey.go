package dtos

type NewWriteKeyResult struct {
	Name string `json:"name"`
	Key  string `json:"key"`
}
