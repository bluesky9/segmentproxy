package dtos

type NewStatementResult struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

type Statement struct {
	Id        int64  `json:"id"`
	OrgId     int64  `json:"orgId"`
	Name      string `json:"name"`
	Type      string `json:"type"`
	Statement string `json:"statement"`
	Active    bool   `json:"active"`
}
