package dtos

type NewEventTypeResult struct {
	Name string `json:"name"`
	Type string `json:"type"`
}
