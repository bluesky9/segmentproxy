package dtos

type UpdateOrgForm struct {
	Name               string `json:"name" binding:"Required"`
	Partition          string `json:"partition"`
	SolutionSignature  string `json:"solution_signature"`
	DomainMapping      string `json:"domain_mapping"`
	SqlPredicate       string `json:"sql_predicate"`
	AssetPrefix        string `json:"asset_prefix"`
	Plan               string `json:"plan"`
	AccentColorLight   string `json:"accent_color_light"`
	AccentColorDark    string `json:"accent_color_dark"`
	HelpUrl            string `json:"help_url"`
	CommunityUrl       string `json:"community_url"`
	SignoutUrl         string `json:"signout_redirect_url"`
	OauthAutoLogin     string `json:"oauth_auto_login"`
	GithubClientId     string `json:"github_client_id"`
	GithubClientSecret string `json:"github_client_secret"`
	GitlabClientId     string `json:"gitlab_client_id"`
	GitlabClientSecret string `json:"gitlab_client_secret"`
	GoogleClientId     string `json:"google_client_id"`
	GoogleClientSecret string `json:"google_client_secret"`
	OauthClientId      string `json:"oauth_client_id"`
	OauthClientSecret  string `json:"oauth_client_secret"`
	OauthAuthUrl       string `json:"oauth_auth_url"`
	OauthTokenUrl      string `json:"oauth_token_url"`
	OauthApiUrl        string `json:"oauth_api_url"`
	OauthTlsClientCert string `json:"oauth_tls_client_cert"`
	OauthTlsClientKey  string `json:"oauth_tls_client_key"`
	OauthSendViaPost   string `json:"oauth_send_via_post"`
}

type UpdateOrgAddressForm struct {
	Address1 string `json:"address1"`
	Address2 string `json:"address2"`
	City     string `json:"city"`
	ZipCode  string `json:"zipcode"`
	State    string `json:"state"`
	Country  string `json:"country"`
}
