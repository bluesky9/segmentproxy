package api

import (
	"github.com/grafana/grafana/pkg/api/dtos"
	"github.com/grafana/grafana/pkg/bus"
	m "github.com/grafana/grafana/pkg/models"
	"github.com/grafana/grafana/pkg/util"
)

func GetStatements(c *m.ReqContext) Response {
	query := m.GetStatementQuery{OrgId: c.OrgId}

	if err := bus.Dispatch(&query); err != nil {
		return Error(500, "Failed to list Event Types", err)
	}

	result := make([]*m.StatementDTO, len(query.Result))
	for i, t := range query.Result {
		result[i] = &m.StatementDTO{
			Id:        t.Id,
			Name:      t.Name,
			Type:      t.Type,
			Active:    t.Active,
			Statement: t.Statement,
		}
	}

	return JSON(200, result)
}

func GetStatementById(c *m.ReqContext) Response {
	query := m.GetStatementByIdQuery{
		Id:    c.ParamsInt64(":id"),
		OrgId: c.OrgId,
	}

	if err := bus.Dispatch(&query); err != nil {
		if err == m.ErrStatementNotFound {
			return Error(404, "Data source not found", nil)
		}
		return Error(500, "Failed to query datasources", err)
	}

	ds := query.Result
	dtos := convertStatmentModelToDtos(ds)

	return JSON(200, &dtos)
}

func DeleteStatement(c *m.ReqContext) Response {
	id := c.ParamsInt64(":id")

	cmd := &m.DeleteStatementCommand{Id: id, OrgId: c.OrgId}

	err := bus.Dispatch(cmd)
	if err != nil {
		return Error(500, "Failed to delete Event Type", err)
	}

	return Success("Event Type key deleted")
}

func UpdateStatement(c *m.ReqContext, cmd m.UpdateStatementCommand) Response {
	cmd.OrgId = c.OrgId
	cmd.Id = c.ParamsInt64(":id")

	//err := fillWithSecureJSONData(&cmd)
	//if err != nil {
	//	return Error(500, "Failed to update statement", err)
	//}

	err := bus.Dispatch(&cmd)
	if err != nil {
		if err == m.ErrStatementUpdatingOldVersion {
			return Error(500, "Failed to update statement. Reload new version and try again", err)
		}
		return Error(500, "Failed to update statement", err)
	}

	query := m.GetStatementByIdQuery{
		Id:    cmd.Id,
		OrgId: c.OrgId,
	}

	if err := bus.Dispatch(&query); err != nil {
		if err == m.ErrStatementNotFound {
			return Error(404, "Statement not found", nil)
		}
		return Error(500, "Failed to query Statements", err)
	}

	stos := convertStatmentModelToDtos(query.Result)

	return JSON(200, util.DynMap{
		"message":   "Statement updated",
		"id":        cmd.Id,
		"name":      cmd.Name,
		"type":      cmd.Type,
		"statement": stos,
	})
}

// Get /api/datasources/id/:name
func GetStatementIdByName(c *m.ReqContext) Response {
	query := m.GetStatementByNameQuery{Name: c.Params(":name"), OrgId: c.OrgId}

	if err := bus.Dispatch(&query); err != nil {
		if err == m.ErrDataSourceNotFound {
			return Error(404, "Statment not found", nil)
		}
		return Error(500, "Failed to query statments", err)
	}

	ds := query.Result
	dtos := dtos.AnyId{
		Id: ds.Id,
	}

	return JSON(200, &dtos)
}

func AddStatement(c *m.ReqContext, cmd m.AddStatementCommand) Response {
	cmd.OrgId = c.OrgId

	if err := bus.Dispatch(&cmd); err != nil {
		return Error(500, "Failed to add Event Type", err)
	}

	result := &dtos.NewStatementResult{
		Name: cmd.Result.Name,
		Type: cmd.Result.Type,
	}

	return JSON(200, result)
}

func convertStatmentModelToDtos(ds *m.Statement) dtos.Statement {
	dto := dtos.Statement{
		Id:        ds.Id,
		OrgId:     ds.OrgId,
		Name:      ds.Name,
		Type:      ds.Type,
		Statement: ds.Statement,
		Active:    ds.Active,
	}
	return dto
}
