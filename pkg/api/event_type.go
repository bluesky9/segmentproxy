package api

import (
	"github.com/grafana/grafana/pkg/api/dtos"
	"github.com/grafana/grafana/pkg/bus"
	"github.com/grafana/grafana/pkg/log"
	m "github.com/grafana/grafana/pkg/models"
)

func GetEventTypes(c *m.ReqContext) Response {
	query := m.GetEventTypesQuery{OrgId: c.OrgId}

	if err := bus.Dispatch(&query); err != nil {
		return Error(500, "Failed to list Event Types", err)
	}

	result := make([]*m.EventTypeDTO, len(query.Result))
	for i, t := range query.Result {
		result[i] = &m.EventTypeDTO{
			Id:        t.Id,
			Name:      t.Name,
			Type:      t.Type,
			Active:    t.Active,
			Template:  t.Template,
			Retention: t.Retention,
		}
	}

	return JSON(200, result)
}

// GET /api/events/types/search
func SearchEventTypes(c *m.ReqContext) Response {
	query, err := searchEventType(c)
	if err != nil {
		return Error(500, "Failed to fetch users", err)
	}

	return JSON(200, query.Result.EventTypes)
}

func SearchEventTypesWithPaging(c *m.ReqContext) Response {
	log.Error2("Smu")
	query, err := searchEventType(c)
	if err != nil {
		return Error(500, "Failed to fetch users", err)
	}

	return JSON(200, query.Result)
}

func searchEventType(c *m.ReqContext) (*m.SearchEventTypeQuery, error) {
	perPage := c.QueryInt("perpage")
	if perPage <= 0 {
		perPage = 1000
	}
	page := c.QueryInt("page")

	if page < 1 {
		page = 1
	}

	searchQuery := c.Query("query")

	query := &m.SearchEventTypeQuery{Query: searchQuery, Page: page, Limit: perPage}
	if err := bus.Dispatch(query); err != nil {
		return nil, err
	}

	/*
		for _, eventType := range query.Result.EventTypes {
			eventType.AvatarUrl = dtos.GetGravatarUrl(user.Email)
		}
	*/
	log.Error2("Smu")

	query.Result.Page = page
	query.Result.PerPage = perPage

	return query, nil
}

func DeleteEventType(c *m.ReqContext) Response {
	id := c.ParamsInt64(":id")

	cmd := &m.DeleteEventTypeCommand{Id: id, OrgId: c.OrgId}

	err := bus.Dispatch(cmd)
	if err != nil {
		return Error(500, "Failed to delete Event Type", err)
	}

	return Success("Event Type key deleted")
}

func AddEventType(c *m.ReqContext, cmd m.AddEventTypeCommand) Response {
	cmd.OrgId = c.OrgId

	if err := bus.Dispatch(&cmd); err != nil {
		return Error(500, "Failed to add Event Type", err)
	}

	result := &dtos.NewEventTypeResult{
		Name: cmd.Result.Name,
		Type: cmd.Result.Type,
	}

	return JSON(200, result)
}
