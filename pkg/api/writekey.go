package api

import (
	"github.com/grafana/grafana/pkg/api/dtos"
	"github.com/grafana/grafana/pkg/bus"
	"github.com/grafana/grafana/pkg/components/writekeygen"
	m "github.com/grafana/grafana/pkg/models"
)

func GetWriteKeys(c *m.ReqContext) Response {
	query := m.GetWriteKeysQuery{OrgId: c.OrgId}

	if err := bus.Dispatch(&query); err != nil {
		return Error(500, "Failed to list write keys", err)
	}

	result := make([]*m.WriteKeyDTO, len(query.Result))
	for i, t := range query.Result {
		result[i] = &m.WriteKeyDTO{
			Id:   t.Id,
			Name: t.Name,
			Key:  t.Key,
		}
	}

	return JSON(200, result)
}

func DeleteWriteKey(c *m.ReqContext) Response {
	id := c.ParamsInt64(":id")

	cmd := &m.DeleteWriteKeyCommand{Id: id, OrgId: c.OrgId}

	err := bus.Dispatch(cmd)
	if err != nil {
		return Error(500, "Failed to delete Write key", err)
	}

	return Success("Write key deleted")
}

func AddWriteKey(c *m.ReqContext, cmd m.AddWriteKeyCommand) Response {
	cmd.OrgId = c.OrgId

	newKeyInfo := writekeygen.New(cmd.OrgId, cmd.Name)
	cmd.Key = newKeyInfo.HashedKey

	if err := bus.Dispatch(&cmd); err != nil {
		return Error(500, "Failed to add Write key", err)
	}

	result := &dtos.NewWriteKeyResult{
		Name: cmd.Result.Name,
		Key:  newKeyInfo.ClientSecret}

	return JSON(200, result)
}
