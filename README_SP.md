This is a custom build for [Grafana](https://grafana.com) for Segment Proxy.

This repo is manually synced with grafana/grafana by the project owner.

#Requirements
  - GO
  - Node & NVM

#Building and run locally
  1. Install go, get grafana and build (steps detailed [here](http://docs.grafana.org/project/building_from_source/))
     - ```sudo apt install golang-go``` (Make sure to install GO 1.11 or above)
     - Go to or create a directory for your go projects
     - ```export GOPATH=`pwd` ```
     - ```go get github.com/grafana/grafana```  (will be replaced later)
     - ```cd $GOPATH/src/github.com/grafana/grafana```
     - ```go run build.go setup```
     - ```go run build.go build```
         
  2. Replace the ...src/github.com/grafana/grafana directory with this content of this repo
     - ```cd $GOPATH/src/github.com/grafana```
     - ```rm -rf grafana```
     - ```git clone https://github.com/segmentproxy/grafana.git``` 
     
  3. Build again and run
     - from a new terminal:
       - Go to or create a directory for your go projects
       - ```export GOPATH=`pwd` ```
       - ```cd $GOPATH/src/github.com/grafana/grafana```
       - ```nvm use 8```
       - ```npm install -g yarn```
       - ```yarn install --pure-lockfile```
       - ```yarn watch``` 
     - from a new terminal:
       - Go to or create a directory for your go projects
       - ```export GOPATH=`pwd` ```
       - ```cd $GOPATH/src/github.com/grafana/grafana```
       - ```go run build.go build```
       - ```./bin/linux-amd64/grafana-server```
     - from browser:
       - [http://localhost:3000/login](http://localhost:3000/login)
       - admin/admin
       - add segmentproxy.localhost to /etc/hosts     
  
#Development mode
  - Set development mode in config
  - Go to or create a directory for your go projects
  - ```export GOPATH=`pwd` ```
  - ```go get github.com/Unknwon/bra``` (This will hot-reload the grafana server on any changes)
  - ```cd $GOPATH/src/github.com/grafana/grafana```
  - ```$GOPATH/src/bin/bra run``` 

#Setup plugins
The Segmentproxy version of Grafana assumes that some plugins are in place.
Please install them before gogin further.
  - Plugins in the ```$GOPATH/src/github.com/grafana/plugins``` directory are automatically picket up  
  - (We are currently preparing the github repos for plugins)